import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConfettiComponent } from './confetti/confetti.component';
import { GaugeComponent } from './gauge/gauge.component';
import {NgxGaugeModule} from "ngx-gauge";
import { StepComponent } from './step/step.component';

@NgModule({
  declarations: [
    AppComponent,
    ConfettiComponent,
    GaugeComponent,
    StepComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgxGaugeModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
