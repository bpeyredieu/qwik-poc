import { Component } from '@angular/core';

@Component({
  selector: 'app-gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.css']
})
export class GaugeComponent {

  gaugeType = "semi";
  gaugeValue = 28.3;
  gaugeLabel = "Speed";
  gaugeAppendText = "km/hr";

  title = 'demo';
  currentValue: number = 0;
  time = 700;
  changeValue() {
    if(this.currentValue == 100) return;
    this.currentValue++;
  }

  ngOnInit() {
      this.currentValue = Math.floor(Math.random() * 100);

  }


}
