import { Component } from '@angular/core';

@Component({
  selector: 'app-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.css']
})
export class StepComponent {
  gettingStartedStep: number = 0;


  GETTING_STARTED_STEPS = [
    {
      message:
        "Press and hold the <b>ALT</b> key to activate 'Click-to-Source' mode",
      hint: ""
    },
    {
      message:
        "Select the title of this page while keeping the <b>ALT</b> key pressed",
      hint: 'Edit the title and save the changes. If your editor does not open, have a look at <a href="https://github.com/yyx990803/launch-editor#supported-editors" target="_blank">this page</a> to set the correct <code>LAUNCH_EDITOR</code> value.',
    },
    {
      message:
        "<b>Update</b> now the <code>routeLoader$</code> defined in the <code>src/routes//layout.tsx</code> file",
      hint: "Instead of returning the current date, you could return any possible string.<br />The output is displayed in the footer.",
    },
    {
      message: "Create a <b>new Route</b> called <code>/me</code>",
      hint: 'Create a new directory called <code>me</code> in <code>src/routes</code>. Within this directory create a <code>index.tsx</code> file or copy the <code>src/routes/index.tsx</code> file. Your new route is now accessible <a href="/me" target="_blank">here</a> ✨',
    },
    {
      message: "Time to have a look at <b>Forms</b>",
      hint: 'Open <a href="/demo/todolist" target="_blank">the TODO list App</a> and add some items to the list. Try the same with disabled JavaScript 🐰',
    },
    {
      message: "<b>Congratulations!</b> You are now familiar with the basics! 🎉",
      hint: "If you need further info on how to use qwik, have a look at <a href='https://qwik.builder.io' target='_blank'>qwik.builder.io</a> or join the <a href='https://qwik.builder.io/chat' target='_blank'>Discord channel</a>.",
    },
  ];

  getCurrentStep() {
    return this.GETTING_STARTED_STEPS[this.gettingStartedStep];
  }


  changeValue() {
    if (this.gettingStartedStep < this.GETTING_STARTED_STEPS.length - 1) {
      this.gettingStartedStep++;
    }else{
      this.gettingStartedStep= 0;
    }
  }


}
